from sklearn.datasets import *
from sklearn.neighbors import KNeighborsClassifier
import numpy as np
import cv2 as cv

neighbors = np.arange(20) + 1
num_of_rows = 16
num_of_column = 12
pixels_per = 128
dataFile = 'handletters.png'

def sweep(neighbours):
	saves = np.array([])

	for i in neighbours:
		img = cv.imread('handletters.png')
		gray = cv.cvtColor(img,cv.COLOR_BGR2GRAY)

		# Now we split the image to 5000 cells, each 20x20 size
		cells = [np.hsplit(row, num_of_rows) for row in np.vsplit(gray, num_of_column)]

		# Make it into a Numpy array: its size will be (50,100,20,20)
		x = np.array(cells)

		# Now we prepare the training data and test data
		x_train = x[:,:8].reshape(-1, pixels_per**2).astype(np.float32)
		x_test = x[:,8:16].reshape(-1, pixels_per**2).astype(np.float32)

		# Create labels for train and test data
		k = np.arange(6)
		y_train = np.repeat(k,16)[:,np.newaxis]
		y_train = y_train.flatten()
		y_test = y_train.copy()

		# Fit
		knn = KNeighborsClassifier(n_neighbors=i)
		knn.fit(x_train, y_train)

		# Predict
		print(knn.score(x_test, y_test)*100)

		#add to saves
		saves = np.append(saves, knn.score(x_test, y_test)*100)
  
	#find best neighbour value
	best = saves.argmax()
	print("best neighbour using sklearn was n: ", neighbors[best], "at", saves[best])

#running
sweep(neighbors)