# using openCV and sklearn version of kNN
this folder contains two python programs that both analyses the picture handletters.png.

In both programs they take a sweep for how far the neighbours should be while training from 1-20. after seeping through the values it then pickes the best one and tells you the k-value for neighbour that corresponds to.

## running the program 
to run the porgrams you need pyhon and both openCV and sklearn for python.

python you can download from its website : https://www.python.org/downloads/

after install you can then add the extensions by running:
```
pip install opencv-python
pip install sklearn
```

after installing the extansions you then run each program induvisually to see the results.

## Contributer
Alexander Øvergård
