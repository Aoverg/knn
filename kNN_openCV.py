import numpy as np
import cv2 as cv

neighbors = np.arange(20) + 1

def sweep(neighbours):

	saves = np.array([])
 
	for i in neighbours:
		img = cv.imread('handletters.png')
		gray = cv.cvtColor(img,cv.COLOR_BGR2GRAY)

		# Now we split the image to 5000 cells, each 20x20 size
		cells = [np.hsplit(row,16) for row in np.vsplit(gray,12)]

		# Make it into a Numpy array: its size will be (50,100,20,20)
		x = np.array(cells)

		# Now we prepare the training data and test data
		x_train = x[:,:8].reshape(-1,128*128).astype(np.float32) # Size = (2500,400)
		x_test = x[:,8:16].reshape(-1,128*128).astype(np.float32) # Size = (2500,400)

		# Create labels for train and test data  
		k = np.arange(6)
		y_train = np.repeat(k,16)[:,np.newaxis]
		y_test = y_train.copy()

		# Initiate kNN, train it on the training data, then test it with the test data with k=1
		knn = cv.ml.KNearest_create()
		knn.train(x_train, cv.ml.ROW_SAMPLE, y_train)
		ret, result, neighbours, dist = knn.findNearest(x_test,k=i)

		# Now we check the accuracy of classification
		# For that, compare the result with test_labels and check which are wrong
		matches = result==y_test
		correct = np.count_nonzero(matches)
		accuracy = correct*100.0/result.size
		print( accuracy )
  
		#add to saves
		saves = np.append(saves, accuracy)
  
	#find best neighbour value
	best = saves.argmax()
	print("best neighbour using openCV was n:", neighbors[best], "at", saves[best])

#running
sweep(neighbors)

